import typing

from blockchain_common.wrapper_block import WrapperBlock
from blockchain_common.wrapper_transaction import WrapperTransaction


class BlockEvent:
    def __init__(self, network_type: str, block: WrapperBlock,
                 transactions_by_address: typing.Dict[str, typing.List[WrapperTransaction]]):
        self.network_type = network_type
        self.block = block
        self.transactions_by_address = transactions_by_address
